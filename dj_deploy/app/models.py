from django.db import models

class Modelo(models.Model):
    codigo = models.CharField("Codigo", max_length=10, unique=True)
    descripciion = models.CharField(max_length=255)

    def __str__(self):
        return self.codigo

    class Meta:
        verbose_name_plural = "Modelos"
